(If your operating system is Window, you are kindly suggested to open all 
the files by using Microsoft Wordpad instead of Microsoft Notepad)

1. File description
There are two sub-directories inside directory “2_Bitstream_generator”:
- Source_code: contains all source code of bitstream generator
- Release: the executable file of bitstream generator.

2. Source_code directory
There are two sub-directories:
- Information_capture: contains source code of main function of bitstream generator.
- file_trans: contains source code of file_transformation_function which is a function will be invoked by bitstream generator.

3. Release directory
Information_capture.exe is the executable file for this module. To use it, double-click on it.



