/*
 ============================================================================
 Name        : file_trans.c
 Author      : XIN
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>



int main(int argc, char* argv[])
{
// lattice workspace

	FILE *fin, *fout;
	char linebuffer[512] = {0};
	char AlgoSize[16] = {0};
	char DataSize[16] = {0};

	char* path1;
	char* path = argv[1];
//path   C:/Users/melin/Desktop/lattice
	path1 = (char*)malloc(sizeof(char)*(strlen(path)+100));
    fin=fopen("FPGA.c","r");

	strcpy(path1,path);
	strcat(path1, "/FPGA.c");


	fout=fopen(path1,"w");

	while(fgets (linebuffer,512,fin) != NULL)
		{
			fprintf(fout,linebuffer);
		}

	fclose(fin);
	fclose(fout);





	FILE* pFILE_FPGA;
	FILE* pFILE_TEST;
	FILE* pFILE_algo;
	FILE* pFILE_data;
	FILE* pFILE_FPGAprj;
    int targetline;
    int i;

    strcpy(path1,path);
    strcat(path1, "/impl1/TEST_FPGA.h");
    pFILE_TEST = fopen(path1,"w+");

    fprintf(pFILE_TEST,"#include <stdint.h>\n\n");
    fprintf(pFILE_TEST,"const uint8_t __fpga_alg[] = {\n");

    strcpy(path1,path);
    strcat(path1,"/impl1/project1_impl1_algo.c");
	pFILE_algo = fopen(path1,"r");
	if(pFILE_algo == NULL)
	{
		printf("open error");
	}
	else
	{
		targetline = 16;
		for(i=0;i<targetline;i++)
		if(fgets (linebuffer,512,pFILE_algo) != NULL)
		sscanf(linebuffer,"%*s%*s%*s%*s%*s%[^;]",AlgoSize);

		//puts (AlgoSize);

		fgets(linebuffer,512,pFILE_algo);
		while(fgets (linebuffer,512,pFILE_algo) != NULL)
		{
			fprintf(pFILE_TEST,"%s",linebuffer);
		}
		fprintf(pFILE_TEST,"\nconst uint8_t __fpga_dat[] = {\n");
	}


	strcpy(path1,path);
	strcat(path1,"/impl1/project1_impl1_data.c");
	pFILE_data = fopen(path1,"r");
	if(pFILE_data == NULL)
		{
			printf("open error");
		}
		else
		{
			targetline = 16;
			for(i=0;i<targetline;i++)
			if (fgets (linebuffer,512,pFILE_data) != NULL){}
			sscanf(linebuffer,"%*s%*s%*s%*s%*s%[^;]",DataSize);
			//puts (DataSize);

			fgets(linebuffer,512,pFILE_data);
			while(fgets (linebuffer,512,pFILE_data) != NULL)
				{
					fprintf(pFILE_TEST,"%s",linebuffer);
				}
		}

	strcpy(path1,path);
	strcat(path1,"/FPGA.c");
	pFILE_FPGA = fopen(path1,"r");
	strcpy(path1,path);
	strcat(path1,"/impl1/FPGA.c");
	pFILE_FPGAprj = fopen(path1,"w+");

	targetline = 8;
	for(i=0;i<targetline;i++)
	if (fgets (linebuffer,512,pFILE_FPGA) != NULL)
	{
		fprintf(pFILE_FPGAprj,linebuffer);
	}

	fprintf(pFILE_FPGAprj,"uint32_t g_iAlgoSize =%s;\n",AlgoSize);
	fprintf(pFILE_FPGAprj,"uint32_t g_iDataSize =%s;\n",DataSize);
	fgets (linebuffer,512,pFILE_FPGA);
	fgets (linebuffer,512,pFILE_FPGA);

	while(fgets (linebuffer,512,pFILE_FPGA) != NULL)
	{
		fprintf(pFILE_FPGAprj,linebuffer);
	}

    fclose(pFILE_FPGA);
    fclose(pFILE_TEST);
    fclose(pFILE_algo);
    fclose(pFILE_data);
    fclose(pFILE_FPGAprj);
    pFILE_FPGA = NULL;
    pFILE_TEST = NULL;
    pFILE_data = NULL;
    pFILE_algo = NULL;
    pFILE_FPGAprj = NULL;
    printf("File tranfromation accomplished");

}
