We provide both pdf files and source files of all the documents

In this directory, there are four files and one directory:

Files:
1. Documentation.pdf
   The documentation of our project
2. User_Manual.pdf
   The user manual to show how to use our program
3. Final_presentation.pdf
   The slides for the final presentation
4. Work_load.pdf
   Indicates work load of each group member.

Directory:
1. Source_file - It contains the source files of those files

