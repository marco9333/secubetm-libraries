library IEEE;
use IEEE.std_logic_1164.all;

entity MUX21_8 is
	Port (	A:	In	std_logic_vector (7 downto 0);
		B:	In	std_logic_vector (7 downto 0);
		SEL:	In	std_logic;
		Y:	Out	std_logic_vector (7 downto 0));
end MUX21_8;

architecture BEHAVIORAL of MUX21_8 is
-- by logical functions
begin

	allbits:for I in 7 downto 0 generate
		bit_I:	Y(I) <= (A(I) and SEL) or (B(I) and not(SEL));
	end generate;

end BEHAVIORAL;
