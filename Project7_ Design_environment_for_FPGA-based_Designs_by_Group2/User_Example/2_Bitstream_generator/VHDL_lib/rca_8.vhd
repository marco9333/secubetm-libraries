library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity RCA_8 is
	Port (	A:	In	std_logic_vector(7 downto 0);
		B:	In	std_logic_vector(7 downto 0);
		Ci:	In	std_logic;
		S:	Out	std_logic_vector(7 downto 0);
		Co:	Out	std_logic);
end RCA_8;

architecture BEHAVIORAL of RCA_8 is
-- I write 2 behavior achitectures for my rca
-- for the 1st one I achieve the add operation by the expression A+B
-- and achieve the carry by 
-- 1.Create 2 new signals A_x, B_x and S_x with the same value as A, B and S
--   while with a length of N+1 bits
-- 2.Calculate sum(S_i) of A_x and B_x
-- 3.Propagate S_i(N-1 downto 0) to S and S_i(N) to Co(carry out)
  signal S_x : std_logic_vector(8 downto 0);
  signal A_x : std_logic_vector(8 downto 0);
  signal B_x : std_logic_vector(8 downto 0);
begin
  A_x <= '0' & A;
  B_x <= '0' & B;
  S_x <= (A_x + B_x + Ci);
  S <= S_x(7 downto 0);
  Co <= S_x(8);
end BEHAVIORAL;


