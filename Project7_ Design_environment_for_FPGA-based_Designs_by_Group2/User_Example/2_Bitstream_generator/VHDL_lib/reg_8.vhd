library IEEE;
use IEEE.std_logic_1164.all;

entity REG_8 is
	Port (	D:	In	std_logic_vector (7 downto 0);
		CK:	In	std_logic;
		RESET:	In	std_logic;
		Q:	Out	std_logic_vector (7 downto 0));
end REG_8;


architecture SYN_BEHAVIOR of REG_8 is -- flip flop D with syncronous reset

begin
	PSYNCH: process(CK,RESET)
	begin
	  if CK'event and CK='1' then -- positive edge triggered:
	    if RESET='1' then -- active high reset 
	      Q <= (others => '0'); 
	    else
	      Q <= D; -- input is written on output
	    end if;
	  end if;
	end process;

end SYN_BEHAVIOR;
