library IEEE;
use IEEE.std_logic_1164.all; --  libreria IEEE con definizione tipi standard logic

entity ND21 is
	Port (	A:	In	std_logic;
		B:	In	std_logic;
		Y:	Out	std_logic);
end ND21;


architecture ARCH1 of ND21 is

begin
	Y <= not( A and B);

end ARCH1;
