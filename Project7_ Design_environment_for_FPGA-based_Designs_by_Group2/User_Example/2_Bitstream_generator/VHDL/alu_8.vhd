----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:42:47 06/19/2017 
-- Design Name: 
-- Module Name:    ALU_8 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

entity ALU_8 is
  port 	 ( FUNC: IN std_logic_vector(3 downto 0);
           DATA1: IN std_logic_vector(7 downto 0);
           DATA2: IN std_logic_vector(7 downto 0);
           OUTALU: OUT std_logic_vector(7 downto 0));
end ALU_8;

architecture BEHAVIORAL of ALU_8 is

begin

P_ALU: process (FUNC, DATA1, DATA2)

  begin
    case FUNC is
	when "0000"	=> OUTALU <= DATA1 + DATA2; -- ADD calculate the sum of two N-bits operands
	when "0001" => OUTALU <= DATA1 - DATA2; -- SUB subtraction of two N-bits data operands
	when "0010"	=> OUTALU <= DATA1 and DATA2; -- BITAND bitwise AND
	when "0011"	=> OUTALU <= DATA1 or DATA2; -- BITOR bitwise OR
	when "0100"	=> OUTALU <= DATA1 xor DATA2; -- BITXOR bitwise XOR
	when "0101"	=> OUTALU <= DATA1(6 downto 0) & '0'; -- FUNCLSL logical shift left, HELP: use the concatenation operator &
	when "0110"	=> OUTALU <= '0' & DATA1(7 downto 1); -- FUNCLSR logical shift right
	when "0111"	=> OUTALU <= DATA1(6 downto 0) & DATA1(7); -- FUNCRL rotate left
	when "1000"	=> OUTALU <= DATA1(0) & DATA1(7 downto 1); -- FUNCRR totate right
	when others => null;
    end case; 
  end process P_ALU;

end BEHAVIORAL;


