#ifndef __cpu_comm_H
#define __cpu_comm_h
#ifdef __cplusplus
 extern "C" {
#endif

	#include "stm32f4xx_hal.h"
	
	void GPIO_ADDR_WRITE(uint16_t addr_6);
	uint16_t GPIO_ADDR_READ();
	void GPIO_DATA_WRITE(int16_t data_16);
	int16_t GPIO_DATA_READ();

#ifdef __cplusplus
}
#endif
#endif