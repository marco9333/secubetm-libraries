(If your operating system is Window, you are kindly suggested to open all 
the files by using Microsoft Wordpad instead of Microsoft Notepad)

1. File description
There are two files inside directory “3_CPU_Communication_Module”:
- cpu_comm.h: header file of the library includes all the function definitions
- cpu_comm.c: c file of the library includes the implementation of functions

2. How to use
User can invoke the functions inside the library by including these two files into Eclipse inside project SEcubeDevBoard under the path “Application/src”.
The in the main file, user can use this library directory.


