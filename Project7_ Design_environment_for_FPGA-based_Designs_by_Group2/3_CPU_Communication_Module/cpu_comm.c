#include "stdlib.h"
#include "math.h"
#include "cpu_comm.h"



/**
 Write pins PF0-PF5
 input: an integer which is a 6-bit address
 */
void GPIO_ADDR_WRITE(uint16_t addr_6)
{
	//	CPU_FPGA_BUS_A(0) 	PF0
	//	CPU_FPGA_BUS_A(1) 	PF1
	//	CPU_FPGA_BUS_A(2) 	PF2
	//	CPU_FPGA_BUS_A(3) 	PF3
	//	CPU_FPGA_BUS_A(4) 	PF4
	//	CPU_FPGA_BUS_A(5) 	PF5
	int16_t  ival, n = 0;
	int addr[6] = {0,0,0,0,0,0,};

	// convert the integer address to an integer array
	ival = addr_6;
	while(ival > 0)
	{
		addr[n++] = ival % 2;
	    ival /= 2;
	}

	if(addr[0]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET);

	if(addr[1]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_1, GPIO_PIN_SET);

	if(addr[2]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_2, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_2, GPIO_PIN_SET);

	if(addr[3]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_3, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_3, GPIO_PIN_SET);

	if(addr[4]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_4, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_4, GPIO_PIN_SET);

	if(addr[5]==0)
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_5, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_5, GPIO_PIN_SET);
}

/**
 Read pins PF0-PF5
 returns: an integer which is a 6-bit address
 */
uint16_t GPIO_ADDR_READ()
{
	//	CPU_FPGA_BUS_A(0) 	PF0
	//	CPU_FPGA_BUS_A(1) 	PF1
	//	CPU_FPGA_BUS_A(2) 	PF2
	//	CPU_FPGA_BUS_A(3) 	PF3
	//	CPU_FPGA_BUS_A(4) 	PF4
	//	CPU_FPGA_BUS_A(5) 	PF5
	int16_t  addr = 0;

	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_0) == GPIO_PIN_SET)
		addr = addr + 1;
	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_1) == GPIO_PIN_SET)
		addr = addr + 2;
	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_2) == GPIO_PIN_SET)
		addr = addr + 4;
	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_3) == GPIO_PIN_SET)
		addr = addr + 8;
	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_4) == GPIO_PIN_SET)
		addr = addr + 18;
	if(HAL_GPIO_ReadPin(GPIOF, GPIO_PIN_5) == GPIO_PIN_SET)
		addr = addr + 32;

	return addr;
}

/**
 Write Data Bus pins
 input: an integer which is a 16-bit data
 */
void GPIO_DATA_WRITE(int16_t data_16)
{
	//	CPU_FPGA_BUS_D(0) 	PD14
	//	CPU_FPGA_BUS_D(1) 	PD15
	//	CPU_FPGA_BUS_D(2) 	PD0
	//	CPU_FPGA_BUS_D(3) 	PD1
	//	CPU_FPGA_BUS_D(4) 	PE7
	//	CPU_FPGA_BUS_D(5) 	PE8
	//	CPU_FPGA_BUS_D(6) 	PE9
	//	CPU_FPGA_BUS_D(7) 	PE10
	//	CPU_FPGA_BUS_D(8) 	PE11
	//	CPU_FPGA_BUS_D(9) 	PE12
	//	CPU_FPGA_BUS_D(10)  PE13
	//	CPU_FPGA_BUS_D(11)	PE14
	//	CPU_FPGA_BUS_D(12)	PE15
	//	CPU_FPGA_BUS_D(13)	PD8
	//	CPU_FPGA_BUS_D(14)	PD9
	//	CPU_FPGA_BUS_D(15)	PD10

	int16_t  ival, i = 0;
	int data[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	// convert the signed integer data to an integer array
	ival = data_16;
	if(ival >= 0)
		data[15] = 0;
	else
		data[15] = 1;

	for(i=14;i>=0;i--)
	{
		ival = ival << 1;
		if(ival >= 0)
			data[i] = 0;
		else
			data[i] = 1;
	}

	if(data[0]==0)
		HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);

	if(data[1]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_15, GPIO_PIN_SET);

	if(data[2]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_SET);

	if(data[3]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);

	if(data[4]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, GPIO_PIN_SET);

	if(data[5]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_SET);

	if(data[6]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_SET);

	if(data[7]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_SET);

	if(data[8]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_SET);

	if(data[9]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_SET);

	if(data[10]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_SET);

	if(data[11]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_SET);

	if(data[12]==0)
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_SET);

	if(data[13]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_8, GPIO_PIN_SET);

	if(data[14]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_9, GPIO_PIN_SET);

	if(data[15]==0)
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, GPIO_PIN_SET);

}

/**
 Read Data Bus pins
 returns: an integer which is a 16-bit data
 */
int16_t GPIO_DATA_READ()
{
	//	CPU_FPGA_BUS_D(0) 	PD14
	//	CPU_FPGA_BUS_D(1) 	PD15
	//	CPU_FPGA_BUS_D(2) 	PD0
	//	CPU_FPGA_BUS_D(3) 	PD1
	//	CPU_FPGA_BUS_D(4) 	PE7
	//	CPU_FPGA_BUS_D(5) 	PE8
	//	CPU_FPGA_BUS_D(6) 	PE9
	//	CPU_FPGA_BUS_D(7) 	PE10
	//	CPU_FPGA_BUS_D(8) 	PE11
	//	CPU_FPGA_BUS_D(9) 	PE12
	//	CPU_FPGA_BUS_D(10)  PE13
	//	CPU_FPGA_BUS_D(11)	PE14
	//	CPU_FPGA_BUS_D(12)	PE15
	//	CPU_FPGA_BUS_D(13)	PD8
	//	CPU_FPGA_BUS_D(14)	PD9
	//	CPU_FPGA_BUS_D(15)	PD10

	int16_t int_data;
	int sign;
	int data[16];

	// sign bit
	if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_10) == GPIO_PIN_SET)
		sign = 0;
	else if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_10) == GPIO_PIN_RESET)
		sign = 1;

	// get the value bits
		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9) == GPIO_PIN_SET)
			data[14] = 1;
		else
			data[14] = 0;

		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8) == GPIO_PIN_SET)
			data[13] = 1;
		else
			data[13] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_15) == GPIO_PIN_SET)
			data[12] = 1;
		else
			data[12] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_14) == GPIO_PIN_SET)
			data[11] = 1;
		else
			data[11] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_13) == GPIO_PIN_SET)
			data[10] = 1;
		else
			data[10] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_12) == GPIO_PIN_SET)
			data[9] = 1;
		else
			data[9] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_11) == GPIO_PIN_SET)
			data[8] = 1;
		else
			data[8] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_10) == GPIO_PIN_SET)
			data[7] = 1;
		else
			data[7] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_9) == GPIO_PIN_SET)
			data[6] = 1;
		else
			data[6] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_8) == GPIO_PIN_SET)
			data[5] = 1;
		else
			data[5] = 0;

		if(HAL_GPIO_ReadPin(GPIOE, GPIO_PIN_7) == GPIO_PIN_SET)
			data[4] = 1;
		else
			data[4] = 0;

		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_1) == GPIO_PIN_SET)
			data[3] = 1;
		else
			data[3] = 0;

		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0) == GPIO_PIN_SET)
			data[2] = 1;
		else
			data[2] = 0;

		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_15) == GPIO_PIN_SET)
			data[1] = 1;
		else
			data[1] = 0;

		if(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14) == GPIO_PIN_SET)
			data[0] = 1;
		else
			data[0] = 0;

		// convert the negative number to positive
		if(sign == 1)
		{
			// neg each bit
			for(int i=0;i<16;i++)
			{
				if(data[i]==0)
					data[i]=1;
				else
					data[i]=0;
			}
			// then +1
			for(int i=0;i<16;i++)
			{
				if(data[i] == 0)
				{
					data[i] = 1;
					break;
				}
				else
				{
					data[i] = 0;
				}
			}
		}

		// convert the binary number to decimal
		if(data[14]==1)
			int_data = int_data + 16384;
		if(data[13]==1)
			int_data = int_data + 8192;
		if(data[12]==1)
			int_data = int_data + 4096;
		if(data[11]==1)
			int_data = int_data + 2048;
		if(data[10]==1)
			int_data = int_data + 1024;
		if(data[9]==1)
			int_data = int_data + 512;
		if(data[8]==1)
			int_data = int_data + 256;
		if(data[7]==1)
			int_data = int_data + 128;
		if(data[6]==1)
			int_data = int_data + 64;
		if(data[5]==1)
			int_data = int_data + 32;
		if(data[4]==1)
			int_data = int_data + 16;
		if(data[3]==1)
			int_data = int_data + 8;
		if(data[2]==1)
			int_data = int_data + 4;
		if(data[1]==1)
			int_data = int_data + 2;
		if(data[0]==1)
			int_data = int_data + 1;

	// convert the negative number back to negative
	if(sign == 1)
		int_data = ~int_data + 1;

	return int_data;
}

