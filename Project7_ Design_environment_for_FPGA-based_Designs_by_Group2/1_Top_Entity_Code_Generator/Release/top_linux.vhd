library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY System IS
    PORT (
          OP_A : in std_logic_vector(7 downto 0);
          OP_B : in std_logic_vector(7 downto 0);
          OP_C : in std_logic_vector(7 downto 0);
          F1 : in std_logic_vector(3 downto 0);
          F2 : in std_logic_vector(3 downto 0);
          OUTPUT : out std_logic_vector(7 downto 0)
          );
END System;

ARCHITECTURE struct OF System IS
    component Alu_8
    PORT (
          FUNC : in std_logic_vector(3 downto 0);
          DATA1 : in std_logic_vector(7 downto 0);
          DATA2 : in std_logic_vector(7 downto 0);
          OUTALU : out std_logic_vector(7 downto 0)
         );
    end component;

signal s_0 : std_logic_vector(3 downto 0);
signal s_1 : std_logic_vector(7 downto 0);
signal s_2 : std_logic_vector(7 downto 0);
signal s_3 : std_logic_vector(7 downto 0);
signal s_4 : std_logic_vector(3 downto 0);
signal s_5 : std_logic_vector(7 downto 0);
signal s_6 : std_logic_vector(7 downto 0);

 begin

s_0 <= F1;
s_1 <= OP_A;
s_2 <= OP_B;
s_3 <= OP_C;
s_4 <= F2;
OUTPUT <= s_6;

Alu_8_1 : Alu_8 port map(FUNC => s_0, DATA1 => s_1, DATA2 => s_2, OUTALU => s_5);
Alu_8_2 : Alu_8 port map(DATA2 => s_3, FUNC => s_4, DATA1 => s_5, OUTALU => s_6);

END struct;
