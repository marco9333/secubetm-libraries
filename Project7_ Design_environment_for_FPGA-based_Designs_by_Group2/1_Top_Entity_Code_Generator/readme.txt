(If your operating system is Window, you are kindly suggested to open all 
the files by using Microsoft Wordpad instead of Microsoft Notepad)

1. File description

There are two sub-directories inside directory “1_Top_Entity_Code_Generator”:
- Source_code: contains all source code of bitstream generator
- Release: the executable file of bitstream generator.

2. Release directory

2.1 Files
—--------------
- run.x - executable file for Linux
- run.exe - executable file for Windows

- IP_init.txt - IP information input file example
- Signal_init.txt - Signal information input file example

- top_linux.vhd - the top-level entity output example for Linux
- top_windows.vhd - the top-level entity output example for Windows

2.2 How to use
—--------------
2.2.1 On Linux

run.x [IP_info_file] [Signal_info_file] [Output_file]

for instance:
./run.x IP_init.txt Signal_init.txt top_linux.vhd

2.2 On Windows

run.exe [IP_info_file] [Signal_info_file] [Output_file]

for instance:
run.exe IP_init.txt Signal_init.txt top_windows.vhd

